# Provider
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      #version = ">= 0.13"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_region
}

data "yandex_iam_service_account" "for-autoscale" {
  name = "for-autoscale"
}

## network
resource "yandex_vpc_network" "vm-network" {
  name = "internal"
}

resource "yandex_vpc_subnet" "dmz" {
  name           = "dmz-1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "app1" {
  name           = "app-1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-network.id
  v4_cidr_blocks = ["192.168.20.0/24"]
}

resource "yandex_vpc_subnet" "app2" {
  name           = "app-2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-network.id
  v4_cidr_blocks = ["192.168.30.0/24"]
}

resource "yandex_vpc_subnet" "db1" {
  name           = "db-1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-network.id
  v4_cidr_blocks = ["192.168.40.0/24"]
}

resource "yandex_vpc_subnet" "db2" {
  name           = "db-2"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-network.id
  v4_cidr_blocks = ["192.168.50.0/24"]
}

resource "yandex_vpc_security_group" "sg-dmz" {
  name                = "sg-dmz"
  network_id          = yandex_vpc_network.vm-network.id
  egress {
    protocol          = "ANY"
    description       = "Inet-out"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
 }

resource "yandex_vpc_security_group" "sg-app1" {
  name                = "sg-app1"
  network_id          = yandex_vpc_network.vm-network.id
  egress {
    protocol          = "ANY"
    description       = "Inet-out"
    v4_cidr_blocks    = ["192.168.30.0/24", "192.168.40.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in"
    v4_cidr_blocks    = ["192.168.30.0/24", "192.168.10.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in-ssh"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 22
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}

resource "yandex_vpc_security_group" "sg-app2" {
  name                = "sg-app2"
  network_id          = yandex_vpc_network.vm-network.id
  egress {
    protocol          = "ANY"
    description       = "Inet-out"
    v4_cidr_blocks    = ["192.168.20.0/24", "192.168.50.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in"
    v4_cidr_blocks    = ["192.168.20.0/24", "192.168.10.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in-ssh"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 22
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}

resource "yandex_vpc_security_group" "sg-db1" {
  name                = "sg-db1"
  network_id          = yandex_vpc_network.vm-network.id
  ingress {
    protocol          = "ANY"
    description       = "Inet-in"
    v4_cidr_blocks    = ["192.168.20.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in-ssh"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 22
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}

resource "yandex_vpc_security_group" "sg-db2" {
  name                = "sg-db2"
  network_id          = yandex_vpc_network.vm-network.id
  ingress {
    protocol          = "ANY"
    description       = "Inet-in"
    v4_cidr_blocks    = ["192.168.30.0/24"]
    port              = 80
  }
  ingress {
    protocol          = "ANY"
    description       = "Inet-in-ssh"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 22
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}

## instance dmz
resource "yandex_compute_instance" "dmz" {
  name        = "dmz"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fdpnu69uindc6eumc"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> hosts.inv;
        :> ip.inv;
        echo [server] >> hosts.inv;
        echo ${yandex_compute_instance.dmz.network_interface.0.nat_ip_address} >> hosts.inv;
        echo ${yandex_compute_instance.dmz.network_interface.0.ip_address} dmz >> ip.inv;
        ssh-keyscan -H ${yandex_compute_instance.dmz.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.dmz.network_interface.0.nat_ip_address};
        ansible-playbook -u ubuntu -i hosts.inv site.yml;
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.dmz.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-dmz.id]
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_vpc_security_group.sg-db2]
}

## instance app1
resource "yandex_compute_instance" "app1" {
  name        = "app1"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fdpnu69uindc6eumc"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> hosts.inv;
        echo [server] >> hosts.inv;
        echo ${yandex_compute_instance.app1.network_interface.0.nat_ip_address} >> hosts.inv;
        echo ${yandex_compute_instance.app1.network_interface.0.ip_address} app1 >> ip.inv;
        ssh-keyscan -H ${yandex_compute_instance.app1.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.app1.network_interface.0.nat_ip_address};
        ansible-playbook -u ubuntu -i hosts.inv site.yml;
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.app1.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-app1.id]
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.dmz]
}

## instance app2
resource "yandex_compute_instance" "app2" {
  name        = "app2"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fdpnu69uindc6eumc"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> hosts.inv;
        echo [server] >> hosts.inv;
        echo ${yandex_compute_instance.app2.network_interface.0.nat_ip_address} >> hosts.inv;
        echo ${yandex_compute_instance.app2.network_interface.0.ip_address} app2 >> ip.inv;
        ssh-keyscan -H ${yandex_compute_instance.app2.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.app2.network_interface.0.nat_ip_address};
        ansible-playbook -u ubuntu -i hosts.inv site.yml;
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.app2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-app2.id]
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.app1]
}

## instance db1
resource "yandex_compute_instance" "db1" {
  name        = "db1"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fdpnu69uindc6eumc"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> hosts.inv;
        echo [server] >> hosts.inv;
        echo ${yandex_compute_instance.db1.network_interface.0.nat_ip_address} >> hosts.inv;
        echo ${yandex_compute_instance.db1.network_interface.0.ip_address} db1 >> ip.inv;
        ssh-keyscan -H ${yandex_compute_instance.app1.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.db1.network_interface.0.nat_ip_address};
        ansible-playbook -u ubuntu -i hosts.inv site.yml;
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.db1.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-db1.id]
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.app2]
}

## instance db2
resource "yandex_compute_instance" "db2" {
  name        = "db2"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8fdpnu69uindc6eumc"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> hosts.inv;
        echo [server] >> hosts.inv;
        echo ${yandex_compute_instance.db2.network_interface.0.nat_ip_address} >> hosts.inv;
        echo ${yandex_compute_instance.db2.network_interface.0.ip_address} db2 >> ip.inv;
        ssh-keyscan -H ${yandex_compute_instance.db2.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.db2.network_interface.0.nat_ip_address};
        ansible-playbook -u ubuntu -i hosts.inv site.yml;

        scp ip.inv files/nginx_host_dmz ubuntu@${yandex_compute_instance.dmz.network_interface.0.nat_ip_address}:/tmp/;
        scp ip.inv files/nginx_host_app1 ubuntu@${yandex_compute_instance.app1.network_interface.0.nat_ip_address}:/tmp/;
        scp ip.inv files/nginx_host_app2 ubuntu@${yandex_compute_instance.app2.network_interface.0.nat_ip_address}:/tmp/;
        scp files/index_db1 ip.inv files/nginx_host_db1 ubuntu@${yandex_compute_instance.db1.network_interface.0.nat_ip_address}:/tmp/;
        scp files/index_db2 ip.inv files/nginx_host_db2 ubuntu@${yandex_compute_instance.db2.network_interface.0.nat_ip_address}:/tmp/;
        ssh ubuntu@${yandex_compute_instance.dmz.network_interface.0.nat_ip_address} "sudo chmod 666 /etc/hosts; cat /tmp/ip.inv >> /etc/hosts; sudo chmod 644 /etc/hosts";
        ssh ubuntu@${yandex_compute_instance.app1.network_interface.0.nat_ip_address} "sudo chmod 666 /etc/hosts; cat /tmp/ip.inv >> /etc/hosts; sudo chmod 644 /etc/hosts";
        ssh ubuntu@${yandex_compute_instance.app2.network_interface.0.nat_ip_address} "sudo chmod 666 /etc/hosts; cat /tmp/ip.inv >> /etc/hosts; sudo chmod 644 /etc/hosts";
        ssh ubuntu@${yandex_compute_instance.db1.network_interface.0.nat_ip_address} "sudo chmod 666 /etc/hosts; cat /tmp/ip.inv >> /etc/hosts; sudo chmod 644 /etc/hosts";
        ssh ubuntu@${yandex_compute_instance.db2.network_interface.0.nat_ip_address} "sudo chmod 666 /etc/hosts; cat /tmp/ip.inv >> /etc/hosts; sudo chmod 644 /etc/hosts";
        ssh ubuntu@${yandex_compute_instance.dmz.network_interface.0.nat_ip_address} "sudo cp /tmp/nginx_host_dmz /etc/nginx/sites-available/default; sudo systemctl reload nginx";
        ssh ubuntu@${yandex_compute_instance.app1.network_interface.0.nat_ip_address} "sudo cp /tmp/nginx_host_app1 /etc/nginx/sites-available/default; sudo systemctl reload nginx";
        ssh ubuntu@${yandex_compute_instance.app2.network_interface.0.nat_ip_address} "sudo cp /tmp/nginx_host_app2 /etc/nginx/sites-available/default; sudo systemctl reload nginx";
        ssh ubuntu@${yandex_compute_instance.db1.network_interface.0.nat_ip_address} "sudo cp /tmp/index_db1 /usr/share/nginx/html/index.html; sudo cp /tmp/nginx_host_db1 /etc/nginx/sites-available/default; sudo systemctl reload nginx";
        ssh ubuntu@${yandex_compute_instance.db2.network_interface.0.nat_ip_address} "sudo cp /tmp/index_db2 /usr/share/nginx/html/index.html; sudo cp /tmp/nginx_host_db2 /etc/nginx/sites-available/default; sudo systemctl reload nginx";
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.db2.id
    nat       = true
    security_group_ids = [yandex_vpc_security_group.sg-db2.id]
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.db1]
}
