####  Как сделать проверку микросервиса с помощью SonerQube в репозитории skillbox-diploma-advanced ?

1. Запускается проверка сервиса сама, автоматически при пуше изменений в репозиторий.
2. В случае нейдачной проверки на почту придёт уведомление.
3. Или можно проверить глазами зайдя в репозиторий на gitlab и вкладку Build -> Pipelines


####  Как проверить свой микросервис с помощью SonerQube на своём компьютере локально ?

1. Нужно перейти https://docs.sonarsource.com/sonarqube/latest/analyzing-source-code/scanners/sonarscanner/  и скачать SonarScanner CLI в соответсвии со своей OS.
2. Распаковываем zip-файл sonar-scanner-*
3. Переходим в распакованную директорию sonar-scanner-*/conf/
4. В файле sonar-scanner.properties добавляем строчку sonar.host.url=http://sonarqube.welcomenews.tk:9000
5. Переходим в директорию своего микросервиса, создаём файл и открываем на редактирование файл sonar-project.properties
6. Добавляем строки
```
   sonar.projectKey=isa:project
   sonar.projectName=skillbox-diploma-advanced
   sonar.projectVersion=1.0
   sonar.sources=.
   sonar.sourceEncoding=UTF-8

Значение sonar.projectKey      можно написать любое
Значение sonar.projectVersion  можно написать любое
```

7. Запускаем сканирование 
```
<path_to_dir>/sonar-scanner-6.0.0.4432-linux/bin/sonar-scanner -Dsonar.projectKey=skillbox-diploma-advanced -Dsonar.sources=. -Dsonar.host.url=http://sonarqube.welcomenews.tk:9000 -Dsonar.token=sqp_7d5ff926ec70787006b62a68df3686968822b961
```
8. Смотрим результаты   http://sonarqube.welcomenews.tk:9000/projects


=============================================================================================================


https://medium.com/@mattpwest/setting-up-sonarqube-with-ansible-fcabadee6953

https://github.com/lrk/ansible-role-sonarqube/blob/master/templates/sonar.properties.j2