
### Финальная работа «DevOps-инженер. Advanced»

В репозитории 1 ветка:
```
- main     ## тут запускается только сборка app приложения и sonar-scanner в Sonerqube
```

1. Для работы нужно 15 виртуальныx машин
   - ВМ Сonsul ( 3 вирт.машины )
   - ВМ Autoscaling Group ( само приложение ) с возможностью горизантального маштабирования
   - ВМ Prometheus, VictoriaMetrics, Grafana, Kibana
   - ВМ EFK ( 3 вирт.машины ), Сurator 
   - ВМ gitlab-runner, Sonerqube
   - ВМ Network ( 5 вирт.машин )
   

2. Перед запуском установить yc, terraform, ansible.

Yandex Cloud (CLI) — скачиваемое программное обеспечение для управления вашими облачными ресурсами через командную строку.
https://cloud.yandex.ru/docs/cli/quickstart#install

Получение OAuth-токена - https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token

Создать SSH key на машине с которй всё запускаем
```shell
 ssh-keygen -t ed25519
```
Передать SSH key на клиентов
```
ssh-copy-id -i <~/.ssh/id_rsa.pub> <username>@<ip_address>
```

Установить pip3, netaddr на ту машину с которой будет запускаться ansible-playbook
```
sudo apt install python3-pip -y
sudo apt install --no-install-recommends python-netaddr
sudo apt install --no-install-recommends python3-netaddr
```

3. Настройка Terraform
- Скопировать файл .terraformrc из директории provider-terraform/ в домашнюю директорию. (~/)

4. Сделать изменения.
- Добавить файлы в /terraform/variables.tf и /terraform/private.auto.tfvars со значениями внутри.

5. Заменить в ветке main по пути skillbox-diploma-advanced/infra/terraform/ansible/roles/gitlab-runner/vars/main.yml
   параметр registration_token на правильный.
  
6. Перейти в директорию skillbox-diploma-advanced/infra/terraform, сделать исполняемый, и запустить скрипт start_terraform.sh для запуска создания ВМ

7. Для запуска ( Задача 8. Сетевая безопасность ) перейти в директорию skillbox-diploma-advanced/network, сделать исполняемый, и запустить скрипт start_terraform.sh для запуска создания ВМ

P.S. При изменении версиии сбрки образа в файле .gitlab-ci.yml, после следует заменить:
- В файле infra/terraform/screept_autoscale.sh в строке 46 на правильную версию
- В файле infra/terraform/ansible/roles/ansible-role-nginx-config/templates/stream/load-balancer.conf.ctmpl.j2 в строке 2 на правильную версию

### Почему лучше не использовать tag latest

https://vsupalov.com/docker-latest-tag/

https://dev.to/renanzulian/why-you-should-stop-to-use-latest-tag-on-docker-b3j

https://habr.com/ru/companies/slurm/articles/452108/ 


### Про кэш и артифакты.
https://mcs.mail.ru/blog/sposoby-keshirovaniya-v-gitlab-ci-rukovodstvo-v-kartinkah


#### Ошибка в гитлабе при сборке
```
Doing these 2 things should fix most scenarios.
in my /etc/gitlab-runner/config.toml file (Updated volumes and privileged):

    privileged = true
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]

```

========================

## Запуск локально на компьютере
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p8080:8080 skillbox/app
   ```
