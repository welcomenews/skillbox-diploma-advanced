```
memory
10 * (100.0-(100 * node_memory_MemAvailable_bytes / node_memory_MemTotal_bytes {instance="158.160.12.39:9100"}))

cpu
floor(100 - (avg by (instance) (rate (node_cpu_seconds_total{mode="idle"}[1m])) * 100))

Disk read and write
rate(node_disk_read_time_seconds_total{instance="158.160.12.39:9100"}[$__rate_interval])
rate(node_disk_write_time_seconds_total{instance="158.160.12.39:9100"}[$__rate_interval])

promhttp_requests_total
promhttp_metric_handler_requests_total


rate(prometheus_http_requests_total[5m])

total memory
node_memory_MemTotal_bytes / 1024 / 1024 /1024

```
====================

```
количество выделенной памяти МБ
go_memstats_alloc_bytes {instance='localhost:8080'} / 1024 / 1024

кол. запросов на сайт
skillbox_http_requests_total

кол. запусков сборщика мусора
go_gc_duration_seconds_count

кол. созданных поток
go_threads {instance='localhost:8080'}

Общее количество запросов по коду состояния HTTP
promhttp_metric_handler_requests_total{code="200", instance='localhost:8080'} 
promhttp_metric_handler_requests_total{code="500", instance='localhost:8080'}
promhttp_metric_handler_requests_total{code="503", instance='localhost:8080'}

```

Если вы не знаете имя сервисного аккаунта, получите список сервисных аккаунтов в каталоге по умолчанию:
```
yc iam service-account list
```

https://github.com/yandex-cloud-examples/yc-vm-group-with-autoscale/blob/main/vm-autoscale.tf

==================

runcmd

https://cloudinit.readthedocs.io/en/22.2/topics/format.html

https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html



