#!/bin/bash

echo -e "P123456\nP123456\n" | passwd root
cd /root/skillbox-diploma-advanced/infra/terraform/
apt-get install python3-pip -y
apt-get install software-properties-common -y
add-apt-repository --yes --update ppa:ansible/ansible
apt-get install ansible -y
apt install sshpass -y
cp ./ssh_config /etc/ssh/
cp ./sshd_config /etc/ssh/
systemctl reload sshd.service
systemctl reload ssh.service
sudo apt install net-tools -y
apt install --no-install-recommends python-netaddr -y
apt install python-pip -y
pip install python-consul
apt-get install ca-certificates curl gnupg lsb-release -y
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
chmod a+r /etc/apt/keyrings/docker.asc
echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io -y
ssh-keygen -t ed25519 -q -N '' -f ~/.ssh/id_ed25519
sshpass -p P123456 ssh-copy-id root@$(cat ip | tail -1 | head -n 1)
sshpass -p P123456 ssh-copy-id root@$(cat ip | tail -2 | head -n 1)
sshpass -p P123456 ssh-copy-id root@$(cat ip | tail -3 | head -n 1)
sshpass -p P123456 ssh-copy-id root@$(cat ip | tail -4 | head -n 1)
sshpass -p P123456 ssh -o StrictHostKeychecking=no root@$(cat ip | tail -1) "ls -l /" > rem
IP_intern=$(hostname -I | awk '{print $1}')
sshpass -p P123456 ssh -o StrictHostKeychecking=no root@${IP_intern} "ls -l /" > rem1
sshpass -p P123456 ssh-copy-id root@${IP_intern}
echo ${IP_intern} consul_bind_address=${IP_intern} consul_client_address=\"${IP_intern} 127.0.0.1\" consul_node_role=client consul_enable_local_script_checks=true >> consul_instances
echo ${IP_intern} >> nginx_instances
echo ${IP_intern} >> consul_services
:>tmhosts.txt
cat consul_instances >> tmhosts.txt
cat nginx_instances >> tmhosts.txt
cat consul_services >> tmhosts.txt
cat consul_template >> tmhosts.txt
sed -i "s/elk_host1/$(cat ip_elk | head -n 2| tail -1)/" ansible/roles/filebeat/files/filebeat.yml
sed -i "s/elk_host2/$(cat ip_elk | head -n 3| tail -1)/" ansible/roles/filebeat/files/filebeat.yml
sed -i "s/elk_host3/$(cat ip_elk | head -n 4| tail -1)/" ansible/roles/filebeat/files/filebeat.yml
sshpass -p P123456 ansible-playbook -e 'ansible_python_interpreter=/usr/bin/python3' -u root -i tmhosts.txt ansible/site.yml |tail -20  > outdeploy
consul kv put release_version latest
docker run -d -p 8080:8080 --name fox registry.gitlab.com/welcomenews/skillbox-diploma-advanced:latest
sed -i "s/1.2.3.4/$(cat ip | tail -1)/g" ansible/nginx-consul-template-ansible.yaml
sleep 10
ansible-playbook -e 'ansible_python_interpreter=/usr/bin/python3' -u root -i tmhosts.txt ansible/nginx-consul-template-ansible.yaml



