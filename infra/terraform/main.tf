# Provider
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
      #version = ">= 0.13"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = var.yc_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_region
}

data "yandex_iam_service_account" "for-autoscale" {
  name = "for-autoscale"
}

## network
resource "yandex_vpc_network" "vm-autoscale-network" {
  name = "internal-2"
}

resource "yandex_vpc_subnet" "vm-autoscale-subnet-b" {
  name           = "internal-b"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.vm-autoscale-network.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}


resource "yandex_vpc_security_group" "sg-1" {
  name                = "sg-autoscale"
  network_id          = yandex_vpc_network.vm-autoscale-network.id
  egress {
    protocol          = "ANY"
    description       = "any"
    v4_cidr_blocks    = ["0.0.0.0/0"]
  }
  ingress {
    protocol          = "TCP"
    description       = "ext-http"
    v4_cidr_blocks    = ["0.0.0.0/0"]
    port              = 80
  }
  ingress {
    protocol          = "TCP"
    description       = "healthchecks"
    predefined_target = "loadbalancer_healthchecks"
    port              = 80
  }
}


## instance 1
resource "yandex_compute_instance" "vm-1" {
  name        = "skillbox-vm1"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :>consul_instances;
        echo [consul_instances] >> consul_instances;
        echo ${yandex_compute_instance.vm-1.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-1.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-1.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        :>ip;
        echo ${yandex_compute_instance.vm-1.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("meta_vm.yml")}"
  }
}

## instance 2
resource "yandex_compute_instance" "vm-2" {
  name        = "skillbox-vm2"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-2.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-2.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-2.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        echo ${yandex_compute_instance.vm-2.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("meta_vm.yml")}"
  }
  depends_on = [yandex_compute_instance.vm-1]
}

## instance 3
resource "yandex_compute_instance" "vm-3" {
  name        = "skillbox-vm3"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-3.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-3.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-3.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=server consul_bootstrap_expect=true >> consul_instances;
        echo ${yandex_compute_instance.vm-3.network_interface.0.ip_address} >> ip
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("meta_vm.yml")}"
  }
  depends_on = [yandex_compute_instance.vm-2]
}

## instance 4
resource "yandex_compute_instance" "vm-4" {
  name        = "skillbox-template"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} consul_bind_address=${yandex_compute_instance.vm-4.network_interface.0.ip_address} consul_client_address=\"${yandex_compute_instance.vm-4.network_interface.0.ip_address} 127.0.0.1\" consul_node_role=client consul_enable_local_script_checks=true >> consul_instances;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> ip;
        :>nginx_instances;
        echo [nginx_instances] >> nginx_instances;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> nginx_instances;
        :>consul_services;
        echo [consul_services] >> consul_services;
        :>consul_template;
        echo [consul_template] >> consul_template;
        echo ${yandex_compute_instance.vm-4.network_interface.0.ip_address} >> consul_template;
        :>tmhosts.txt;
        cat consul_instances >> tmhosts.txt;
        cat nginx_instances >> tmhosts.txt;
        cat consul_template >> tmhosts.txt
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
    user-data = "${file("meta_vm.yml")}"
  }
  depends_on = [yandex_compute_instance.vm-3]
}

## instance elk1
resource "yandex_compute_instance" "elk-1" {
  name        = "skillbox-elk1"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88bokmvjups3o0uqes"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :>ip_elk;
        :>ip_elk_nat;
        echo [ip_elk] > ip_elk_nat;
        echo [ip_elk] > ip_elk;
        echo ${yandex_compute_instance.elk-1.network_interface.0.ip_address} >> ip_elk;
        echo ${yandex_compute_instance.elk-1.network_interface.0.nat_ip_address} >> ip_elk_nat
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.vm-4]
}


## instance elk2
resource "yandex_compute_instance" "elk-2" {
  name        = "skillbox-elk2"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88bokmvjups3o0uqes"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.elk-2.network_interface.0.ip_address} >> ip_elk;
        echo ${yandex_compute_instance.elk-2.network_interface.0.nat_ip_address} >> ip_elk_nat
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.elk-1]
}

## instance elk3
resource "yandex_compute_instance" "elk-3" {
  name        = "skillbox-elk3"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 4
  }

  boot_disk {
    initialize_params {
      image_id = "fd88bokmvjups3o0uqes"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        echo ${yandex_compute_instance.elk-3.network_interface.0.ip_address} >> ip_elk;
        echo ${yandex_compute_instance.elk-3.network_interface.0.nat_ip_address} >> ip_elk_nat;
        git add ip_elk ip_elk_nat ip consul_instances nginx_instances consul_services consul_template;
        git commit -m "refactor ip_elk, ip_elk_nat, refactor ip, consul_instances, nginx_instances, consul_services, consul_template";
        git push;
        sleep 10
    EOT
  }

## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }

  provisioner "local-exec" {
    command = <<EOT
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.elk-1.network_interface.0.nat_ip_address};
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.elk-2.network_interface.0.nat_ip_address};
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.elk-3.network_interface.0.nat_ip_address};
        scp ip_elk ubuntu@$(cat ip_elk_nat | head -n 2| tail -1):/tmp/;
        scp ip_elk ubuntu@$(cat ip_elk_nat | head -n 3| tail -1):/tmp/;
        scp ip_elk ubuntu@$(cat ip_elk_nat | head -n 4| tail -1):/tmp/;
        ansible-playbook -u ubuntu -i ip_elk_nat ansible/monitoring-log.yml
    EOT
  }
  depends_on = [yandex_compute_instance.elk-2]
}


## instance 5
resource "yandex_compute_instance" "vm-5" {
  name        = "skillbox-monitoring"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> monitoring_instances;
        echo [monitoring] >> monitoring_instances;
        echo ${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address} >> monitoring_instances;
        ssh-keyscan -H ${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address};
        scp ip_elk ubuntu@${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address}:/tmp/;
        ansible-playbook -u ubuntu -i monitoring_instances ansible/prometheuse-mon.yml
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [yandex_compute_instance.elk-3]
}

## Create DNS zone
resource "yandex_dns_zone" "zone1" {
  name        = "my-public-zone"
  description = "desc"
  zone        = "welcomenews.tk."
  public      = true
}

### Создание DNS записи monitoring
resource "yandex_dns_recordset" "rs1" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "monitoring.welcomenews.tk."
  type    = "A"
  ttl     = 200
  data  = [yandex_compute_instance.vm-5.network_interface.0.nat_ip_address]
}

### Создание DNS записи grafana
resource "yandex_dns_recordset" "rs2" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "grafana.welcomenews.tk."
  type    = "A"
  ttl     = 200
  data  = [yandex_compute_instance.vm-5.network_interface.0.nat_ip_address]
}

resource "yandex_compute_instance_group" "autoscale-group" {
    name = "autoscale-vm-ig"
    service_account_id  = data.yandex_iam_service_account.for-autoscale.id
    instance_template {

    platform_id = "standard-v3"
    resources {
#      core_fraction = 20
      memory = 2
      cores  = 2
    }
  
    boot_disk {
      initialize_params {
        image_id = "fd8ano8tvtkcdkeimscq"
        size     = 20
      }
    }

## Делает машину не прирываемой.
    scheduling_policy {
      preemptible = false 
    }

    network_interface {
      network_id = yandex_vpc_network.vm-autoscale-network.id
      subnet_ids = ["${yandex_vpc_subnet.vm-autoscale-subnet-b.id}"]
      nat = true
    }

    metadata = {
      ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
      user-data = "${file("meta_autoscale.yml")}"
    }
  }

  scale_policy {
    auto_scale {
      initial_size           = 1
      measurement_duration   = 60
      cpu_utilization_target = 60
      min_zone_size          = 1
      max_size               = 3
      warmup_duration        = 60
    }
  }

  allocation_policy {
    zones = [
##      "ru-central1-a",
      "ru-central1-b"
    ]
  }

  deploy_policy {
    max_creating    = 1
    max_unavailable = 1
    max_expansion   = 0
  }

  load_balancer {
    target_group_name        = "auto-group-tg"
    target_group_description = "load balancer target group"
  }
  depends_on = [yandex_compute_instance.vm-5]
}

# Создание сетевого балансировщика

resource "yandex_lb_network_load_balancer" "balancer" {
  name = "group-balancer"

  listener {
    name        = "http"
    port        = 80
    target_port = 80
  }

  attached_target_group {
    target_group_id = yandex_compute_instance_group.autoscale-group.load_balancer.0.target_group_id
    healthcheck {
      name = "tcp"
      tcp_options {
        port = 80
      }
    }
  }
  depends_on = [yandex_compute_instance_group.autoscale-group]
}

resource "null_resource" "monitoring" {
  
  provisioner "local-exec" {
    command = "sleep 5"
  }

 provisioner "local-exec" {
   command = <<EOT
   ssh-keyscan -H ${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
   ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.vm-5.network_interface.0.nat_ip_address};
   ansible-playbook -u ubuntu -i monitoring_instances ansible/monitoring.yml
   EOT

 }
  depends_on = [yandex_lb_network_load_balancer.balancer]
}

## instance SonarQube
resource "yandex_compute_instance" "sonarqube" {
  name        = "skillbox-testing"
  platform_id = "standard-v3"

  resources {
    # Данный параметр позволяет уменьшить производительность CPU и сильно
    # уменьшить затраты на инфраструктуру
    core_fraction = 20
    cores         = 2
    memory        = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd8ano8tvtkcdkeimscq"
      type     = "network-hdd"
      size     = 10
    }
  }
  provisioner "local-exec" {
    command = <<EOT
        :> sonar.inv;
        echo [server] >> sonar.inv;
        echo ${yandex_compute_instance.sonarqube.network_interface.0.nat_ip_address} >> sonar.inv;
        ssh-keyscan -H ${yandex_compute_instance.sonarqube.network_interface.0.nat_ip_address} >> ~/.ssh/known_hosts;
        ssh-copy-id -i ~/.ssh/id_ed25519.pub -o StrictHostKeychecking=no ubuntu@${yandex_compute_instance.sonarqube.network_interface.0.nat_ip_address};
        ansible-playbook -e 'ansible_python_interpreter=/usr/bin/python3' -u ubuntu -i sonar.inv ansible/sonarqube.yml
    EOT
  }
 
## Делает машину не прирываемой.
  scheduling_policy {
    preemptible = false 
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm-autoscale-subnet-b.id
    nat       = true
  }

  metadata = {
    ssh-keys = "sergey:${file("~/.ssh/id_ed25519.pub")}"
  }
  depends_on = [null_resource.monitoring]
}

### Создание DNS записи sonarqube
resource "yandex_dns_recordset" "rs3" {
  zone_id = yandex_dns_zone.zone1.id
  name    = "sonarqube.welcomenews.tk."
  type    = "A"
  ttl     = 200
  data  = [yandex_compute_instance.sonarqube.network_interface.0.nat_ip_address]
}
