#!/bin/bash

# Initialise the configuration
terraform init -input=false
# Deploy
terraform plan -input=false -out=tfplan
terraform apply tfplan
