FROM golang:1.16.6-alpine3.14 as builder
RUN mkdir /build
WORKDIR /build

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -o app cmd/server/app.go

FROM alpine:3.14

RUN apk add --no-cache --virtual .build-deps go

RUN mkdir /tests

COPY --from=builder /build/app .
COPY --from=builder /build/cmd/server/app.go /test

EXPOSE 8080

CMD ["./app"]
