#!/bin/bash

docker run -d --name fox \
    -v $(pwd):/tests \
    registry.gitlab.com/welcomenews/skillbox-diploma-advanced:latest

docker exec -it fox /tests/run-fmt
docker rm -f fox
